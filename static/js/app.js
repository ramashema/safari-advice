const isWarrantHolder = document.querySelector("#warrantHolder");
const isStationHead = document.querySelector("#stationHead");

isWarrantHolder.addEventListener("change", function (){
    if(isWarrantHolder.value === "YES"){
        isStationHead.value = "YES";
    }
});
isStationHead.addEventListener("change", function (){
    if(isStationHead.value === "NO"){
        isWarrantHolder.value = "NO";
    }
});



