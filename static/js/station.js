const paymentManager = document.querySelector("#paymentManager");
const paymentOffice = document.querySelector("#paymentOffice");
const questionAboutPaymentStation = document.querySelector("#questionAboutPaymentStation");
const paymentManagementStation = document.querySelector("#paymentManagementStation");
const paymentOfficeStation = document.querySelector("#paymentOfficeStation");



// Prevent the payments offices from destructing users when try to create station
questionAboutPaymentStation.addEventListener("change", function (){
    if(questionAboutPaymentStation.value === "YES"){
        paymentManagementStation.classList.remove("dont-display");
        paymentOfficeStation.classList.remove("dont-display");
    } else {
        paymentManagementStation.classList.add("dont-display");
        paymentOfficeStation.classList.add("dont-display");
    }
});

paymentManager.addEventListener("change", function (){
    if(paymentManager.value === "YES"){
        paymentOffice.value = "NO";
        console.log("PaymentOfficeValue: "+ paymentOffice.value);
        paymentOfficeStation.classList.add("dont-display");
    } else {
        paymentManager.value = "NO";
        console.log("PaymentManagerValue: "+ paymentManager.value);
        paymentOfficeStation.classList.remove("dont-display");
    }
});

paymentOffice.addEventListener("change", function (){
    if(paymentOffice.value === "YES"){
        paymentManager.value = "NO";
        console.log("PaymentManagerValue: "+ paymentManager.value);
        paymentManagementStation.classList.add("dont-display");
    } else{
        paymentOffice.value = "NO";
        console.log("PaymentOfficeValue: "+ paymentOffice.value);
        paymentManagementStation.classList.remove("dont-display");
    }
});



