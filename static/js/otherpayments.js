const paymentName = document.querySelector("#payment-name");
const amount = document.querySelector("#amount");
const otherPayAmount = document.querySelector("#otherPayAmount");

paymentName.addEventListener("change", function (){
    if(paymentName.value === "Internal Transport"){
        // amount.classList.add("dont-display");
        otherPayAmount.classList.add("dont-display");
    } else{
        otherPayAmount.classList.remove("dont-display");
    }
});