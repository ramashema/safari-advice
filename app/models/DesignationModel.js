const mongoose = require("mongoose");
const { Schema } = mongoose;

const designationSchema = new Schema({
    name: { type: String, required: [true, "Designation name is required"] },
    description: String,
    institution: { type: Schema.Types.ObjectId, ref: "Institution" },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date,
    deletedAt: Date,
    deleted: { type: Boolean, default: false }
});

const DesignationModel = mongoose.model("Designation", designationSchema);
module.exports = DesignationModel;