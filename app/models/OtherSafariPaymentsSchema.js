const mongoose = require("mongoose");
const { Schema } = mongoose;


const otherPaymentsSchema = new Schema({
    name: { type: String, required: [true, "Specified the name of the payment you request"] },
    description: { type: String, required: [true, "Payment description is required"] },
    amount: Number,
    createdAt:{ type: Date, default: Date.now }, 
    updatedAt: { type: Date },
    deleted: { type: Boolean, default: false }
});

module.exports = otherPaymentsSchema;
