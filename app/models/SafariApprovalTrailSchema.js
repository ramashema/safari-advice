const mongoose = require("mongoose");
const { Schema } = mongoose;


const safariApprovalTrailSchema = new Schema({
    approvalStage: { type: String, required: [true, "Approval stage is required"] },
    approvalStatus: String,
    date: { type: Date, default: Date.now },
    deleted: { Type: Boolean, default: false  }
});

module.exports = safariApprovalTrailSchema;