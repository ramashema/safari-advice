const mongoose = require("mongoose");
const { Schema } = mongoose;

const ratePerdiemSchema = new Schema({
    amount: { type: Number, required: [true, "PerDiem Amount is required"] },
    description: String,
    institution: { type: Schema.Types.ObjectId, ref: "Institution" },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date,
    deletedAt: Date,
    deleted: { type: Boolean, default: false }
});

const RatePerdiemModel = mongoose.model("RatePerdiem", ratePerdiemSchema);
module.exports = RatePerdiemModel;