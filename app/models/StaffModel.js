const mongoose = require("mongoose");
const { Schema } = mongoose;
require("mongoose-type-email");
const bcrypt = require("bcrypt");


const SALT_FACTOR = 10;
const staffSchema = new Schema({
    firstName: String,
    secondName: String,
    lastName: String,
    email: {
        type: mongoose.SchemaTypes.Email,
        required: [true, "Email is required"],
        unique: true
    },
    password: { 
        type: String, 
        required: [true, "User password is required"],
        minlength: [8, "Password must be at least 8 characters long"],
    },
    phoneNumber: { type: String, },
    designation: {
        type: Schema.Types.ObjectId,
        ref: "Designation"
    },
    perdiemRate: {
        type: Schema.Types.ObjectId,
        ref: "RatePerdiem"
    },
    profileImage: String,
    isSysAdmin: { type: Boolean, default: true, },
    isInstituteAdmin: { type: Boolean, default: false, },
    institution: { type: Schema.Types.ObjectId, ref: "Institution" },
    station: { type: Schema.Types.ObjectId, ref: "Station" },
    isStationHead: { type: Boolean, default: false },
    isWarrantHolder: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, },
});

//display user fullName
staffSchema.methods.getFullName = function (){
    if(this.secondName){
        return `${this.lastName}, ${this.firstName} ${this.secondName[0].toUpperCase()}.`;
    } else {
        return `${this.firstName} ${this.lastName}`;
    }
};

//everything to be done before saving the staff
staffSchema.pre("save", function (next){
    let staff = this;
    //check if user password is modified
    if(!staff.isModified("password")){
        return next();
    }
    //generate salt and hash password if user password is modified
    bcrypt.genSalt(SALT_FACTOR, function (error, salt){
        if(error){
            return next(error);
        }
        bcrypt.hash(staff.password, salt, function(error, hashedPassword){
            if(error){
                return next(error);
            }
            staff.password = hashedPassword;
            next();
        });
    });
});

//compare password during login process
staffSchema.methods.checkPassword = function(suppliedPassword, next){
    bcrypt.compare(suppliedPassword, this.password, function(error, isMatch){
        next(error, isMatch);
    });
};


const Staff = mongoose.model("Staff", staffSchema);
module.exports = Staff;
