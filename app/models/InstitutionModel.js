const mongoose = require("mongoose");
const { Schema } = mongoose;


const institutionSchema = new Schema({
    name: {
        type: String,
        unique: [true, "Institution with the same name already exist in the system!"],
        required: [true, "Institution must have a name"]
    },
    staff: { // institution administrator
        type: Schema.Types.ObjectId,
        ref: "Staff",
    },
    description: String,
    shortCode: {
        type: String,
        required: [true, "The institution shortcode is required"]
    },
    logoImageURL: {
        type: String,
    },
    createAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, },
    deleted: { type: Boolean, default: false }
});

const InstitutionModel = mongoose.model("Institution", institutionSchema);
module.exports = InstitutionModel;


