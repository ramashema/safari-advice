const mongoose = require("mongoose");
const { Schema } = mongoose;

const OtherSafariPayment = require("./OtherSafariPaymentsSchema");
const SafariApprovalTrail = require("./SafariApprovalTrailSchema");

const safariSchema = new Schema({
    staff: { type: Schema.Types.ObjectId, ref: "Staff" },
    destination: { type: String,
        required: [true, "Travel Destination is required"],
        minlength: [3, "Correct location is required"]
    },
    isHalfPerDiem: {
        type: Boolean,
        required: [true, "Specify this safari involve half perdiem payments"],
        default: false
    },
    isOfficial: {
        type: Boolean,
        required: [true, "Safari can either be official of private"]
    },
    purpose: {
        type: String,
        required: [true, "Please specify the purpose of your travel"]
    },
    departureDate: {
        type: Date,
        required: [true, "When are you leaving?"]
    },
    returnDate: {
        type: Date,
        required: [true, "When are you returning?"],
    },
    permissionStation: {
        type: Schema.Types.ObjectId,
        ref: "Station",
        required: [true, "The permission station is required for approval process "]
    },
    warrantHoldingStation: {
        type: Schema.Types.ObjectId,
        ref: "Station",
        required: [true, "Warrant Holding station is required for payment approval process"]
    },
    otherPayments: [OtherSafariPayment],
    approvalStage: {
        type: String,
        required: [true, "Every safari advice should have an approval stage"],
    },
    approval: { type: String, required: true, default: "pending" },
    approvedBy: String,
    forwardedBy: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, },
    deleted: { type: Boolean, default: false },
    cancelled: { type: Boolean, default: false },
    approvalHistory: [ SafariApprovalTrail ]
});

const SafariModel = mongoose.model("Safari", safariSchema);
module.exports = SafariModel;
