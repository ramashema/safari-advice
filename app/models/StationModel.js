//The Station is an organization unit, could be an office, unit, directorate or department
const mongoose = require("mongoose");
const { Schema } = mongoose;

const stationSchema = new Schema({
    name: {
        type: String,
        required: [true, "The work station must have a name"],
    },
    shortCode: { type: String, required: [true, "The station shortcode is required"] },
    headOfStationTitle: {// the title of the head of office
        type: String,
        required: [true, "Title of the head of the office is required"]
    },
    isTopStation: { type: Boolean },
    isPaymentManager: { type: Boolean, default: false },
    isCashOffice: { type: Boolean, default: false },
    topStation: { type: Schema.Types.ObjectId, ref: "Station" },
    institution: {
        type: Schema.Types.ObjectId,
        ref: "Institution",
        required: [true, "Institution for the station is missing"]
    },
    createAt: { type: Date, default: Date.now },
    updateAt: { type: Date },
    deleted: { type: Boolean, default: false }
});

const StationModel = mongoose.model("Station", stationSchema);
module.exports = StationModel;

