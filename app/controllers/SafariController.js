const Safari = require("../models/SafariModel");
const Station = require("../models/StationModel");
const Institution = require("../models/InstitutionModel");
const {validationResult} = require("express-validator");
const Staff = require("../models/StaffModel");


/***
 * Approve Safari - utility function
 * @param safari
 * @param request
 */
function approve(safari, request) {
    safari.approval = "approved";
    safari.approvedBy = `${request.user.firstName} ${request.user.lastName}`;
    safari.approvalHistory.push({
        approvalStage: safari.approvalStage,
        approvalStatus: "approved",
        date: new Date()
    });
    safari.markModified("approval");
    safari.markModified("approvedBy");
}

function checkDuplicatedSafari(request, response, departureDate, returnDate, today){
    if(departureDate.getTime() > returnDate.getTime() || departureDate.getTime() < today.getTime()){
        request.flash("error", "Something is wrong with the departure and return dates." +
            " Departure date can not be in future of return date or in the past of the current date(i.e. TODAY)");
        return response.redirect("back");
    }
}

/**
 * Launch safari advice form
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function createSafari(request, response, next){
    if(!request.user.station){
        request.flash("error", "No station allocation,Please contact system administrator, " +
            "if you're one please update your profile before trying to create any safari");
        return response.redirect("/home");
    }

    const destinations = [
        "Dar es Salaam", "Coastal Region", "Morogoro", "Tanga", "Kilimanjaro", "Arusha", "Manyara", "Singida", "Dodoma",
        "Iringa", "Njombe", "Songea", "Mbeya", "Rukwa", "Katavi", "Kigoma", "Tabora", "Shinyanga", "Kahama", "Mwanza"
    ];
    Institution.findById(request.user.institution , function (error, institution){
       if(error){ return next(error); }
       if(!institution){
           request.flash("error", "Something went wrong, there was an error when connecting you with your institution, " +
               "please contact administrator");
           return response.redirect("/home");
       }

       // here is the code that use promises instead of call backs
       const promise = Station.find({institution: institution}).exec();
        promise.then(stations => {
           // console.log(stations);
           if(stations.length === 0){
               request.flash("error", "Something went wrong, we can't find stations. please contact administrator");
               return response.redirect("/home");
           }

           // get current user station
           const userStation = stations.filter( station =>{
               return station.id === String(request.user.station);
           })[0];

           return response.render("create-new-safari", {
               csrfToken: request.csrfToken(),
               destinations: destinations,
               userStation: userStation,
               stations: stations,
               institution: institution,
           });
       });
        promise.catch(error =>{
           console.log(error.message);
       });
       // Station.find({ institution: institution }).populate("topStation").exec(function (error, stations){
       //     if(error){ return next(error); }
       //
       //     if(stations.length === 0){
       //         request.flash("error", "Something went wrong, we can't find stations. please contact administrator");
       //         return response.redirect("/home");
       //     }
       //
       //     // get current user station
       //     const userStation = stations.filter( station =>{
       //         return station.id === String(request.user.station);
       //     })[0];
       //
       //     return response.render("create-new-safari", {
       //         csrfToken: request.csrfToken(),
       //         destinations: destinations,
       //         userStation: userStation,
       //         stations: stations,
       //         institution: institution,
       //     });
       //  });
    });

}
/**
 * Process safari new safari creation
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function processCreationOfSafari(request, response, next){
    if(!request.user.station){
        request.flash("error", "No station allocation,Please contact system administrator, if you're one please update" +
            " your profile trying to create any safari");
        return response.redirect("/home");
    }

    Station.findById(request.user.station, function (error, station){
        if(error){ return next(error); }

        //check form validation errors and display the back to the users
        const errors = validationResult(request);
        if(!errors.isEmpty()){
            let errorMessages = [];
            errors.array().forEach(error => {
                errorMessages.push(error.msg);
            });
            request.flash("error", errorMessages);
            return response.redirect("back");
        }

        // prepare dates for further validations
        const departureDate = new Date(request.body.departuredate);
        const returnDate = new Date(request.body.returndate);
        const today = new Date();
        let permissionStation;

        // check who is submitting the safari to determine the permission station
        if(!request.user.isStationHead){
            permissionStation = request.user.station;
        }else if(request.user.isStationHead && station.isTopStation){
            permissionStation = request.body.permissionStation;
        }else {
            permissionStation = station.topStation;
        }

        checkDuplicatedSafari(request, response, departureDate, returnDate, today);

        Safari.find({ staff: request.user,
            deleted: false,
            departureDate: { $lte: departureDate },
            returnDate: { $gte: departureDate } },
            function (error, safaries){
            if(error){ next(error); }

            if(safaries.length > 0){
                request.flash("error", "There is a collision between your safaries, please revise departure and return dates");
                return response.redirect("back");
            }

            const newSafari = new Safari({
                staff: request.user,
                destination: request.body.destination,
                isOfficial: request.body.isOfficial === "YES",
                isHalfPerDiem: request.body.isHalfPerDiem === "YES",
                purpose: request.body.purpose,
                departureDate: request.body.departuredate,
                returnDate: request.body.returndate,
                permissionStation: permissionStation,
                warrantHoldingStation: request.body.warrantHoldingStation,
                approvalStage: "supervisor"
            });

            newSafari.save(function (error){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }
                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }
                if(newSafari.isOfficial){
                    //adding approval history for safari advice
                    newSafari.approvalHistory.push({ approvalStage: newSafari.approvalStage });
                    newSafari.markModified("approvalHistory");
                    newSafari.save(function (error){
                        if(error){
                            let errorMsg = [];
                            for(let key in error.errors){
                                errorMsg.push(error.errors[key].message);
                            }
                            request.flash("error", errorMsg);
                            return response.redirect("back");
                        }
                    });

                    return response.render("confirm-other-payments",
                        { infos: ["Your safari is successfully created!"],
                            safari: newSafari, staff: request.user
                        });
                }
                request.flash("info", "Your safari is successfully created!");
                return response.redirect("/home");
            });
        });
    });
}

/***
 * Create other payments for the safari
 * @param request
 * @param response
 * @return {*}
 */
function createOtherPayments(request, response){
    const otherPaymentsList = [
        "Bus Fare", "Internal Transport",
        "Entertainment Allowance", "On transit",
        "Fuel", "Accommodation",
        "Fee"
    ];
    if(request.user.id !== request.params.staffId){
        request.flash("error", "Unauthorized action!");
        return response.redirect("/home");
    }
    return response.render("create-other-payments", {
        csrfToken: request.csrfToken(),
        safariId: request.params.safariId,
        otherPaymentsList: otherPaymentsList
    });
}

/**
 * Process creation of other payments for safari advice
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function processCreationOfOtherPayments(request, response, next){
    const errors = validationResult(request);
    if(!errors.isEmpty()){
        let errorMessages = [];
        errors.array().forEach( error => {
            errorMessages.push(error.msg);
        });
        request.flash("error", errorMessages);
        return response.redirect("back");
    }

    Safari.findById(request.body.safariId, function (error, safari){
        let amount = request.body.amount;
        let internalTransportRate = 30000;

        if(error){ return next(error); }
        if(safari.otherPayments){
           const sameOtherPayment = safari.otherPayments.filter( otherPayment => {
               return otherPayment.name === request.body.paymentname;
           });

           if(sameOtherPayment.length > 0){
               request.flash("error", `${request.body.paymentname} already exist in this safari`);
               return response.redirect("back");
           }

           if(request.body.paymentname === "Internal Transport"){
               const departureDate = new Date(safari.departureDate);
               const returnDate = new Date(safari.returnDate);

               const numberOfNights = (returnDate.getTime() - departureDate.getTime()) / (1000 * 60 * 60 * 24);
               amount = Number(numberOfNights) * internalTransportRate;
           }

           const newOtherPayment = {
               name: request.body.paymentname,
               description: request.body.description,
               amount: amount
           };

           safari.otherPayments.push(newOtherPayment);
           safari.markModified("otherPayments");
           safari.save(function(error){
               if(error){
                  let errorMessages = [];
                  for(let key in error.errors){
                      errorMessages.push(error.errors[key].message);
                  }
                  request.flash("error", errorMessages);
                  return response.redirect("back");
               }
               return response.render("add-more-other-payments", {
                   safariId: safari.id,
                   staffId: request.user.id,
                   infos: [`${newOtherPayment.name} added to your current safari`]
               });
           });
        }
    });
}

/**
 * loadSafari advices for approval process
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function loadSafariForApproval(request, response, next){
    if(!request.user.isStationHead){
        request.flash("error", "Unauthorized action");
        return response.redirect("/home");
    }

    Institution.findById(request.user.institution, function (error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, please contact system administrator");
            return response.redirect("back");
        }

        Station.findById(request.user.station, function (error, station){
            Safari.find({deleted: false, cancelled: false}).populate("staff")
                .populate("permissionStation")
                .populate("warrantHoldingStation")
                .exec(function (error, safaries){
                // Permission approval
                if(request.params.approvalStage === "supervisor"){
                    const safariForPermissionApproval = safaries.filter(safari => {
                        return safari.approvalStage === "supervisor" && safari.permissionStation.id === station.id;
                    });

                    return response.render("safari-for-supervisor-approval", {
                        safaries: safariForPermissionApproval,
                        station: station,
                        institution: institution
                    });
                }

                if(request.params.approvalStage === "financial_approval"){
                    if(!request.user.isWarrantHolder){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("back");
                    }

                    const safariForFinancialApproval = safaries.filter(safari => {
                        return safari.approvalStage === "financial_approval" && safari.warrantHoldingStation.id === station.id && safari.isOfficial;
                    });

                    return response.render("safari-for-financial-approval", {
                        safaries: safariForFinancialApproval,
                        station: station,
                        institution: institution
                    });
                }

                if(request.params.approvalStage === "payment"){
                    if(error){ return next(error); }
                    if(!station){
                        request.flash("error", "Something went wrong, please contact system administrator");
                        return response.redirect("back");
                    }

                    if(!station.isPaymentManager){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("back");

                    }

                    const safariForPaymentApproval = safaries.filter(safari => {
                        return safari.approvalStage === "payment" && safari.isOfficial;
                    });

                    return response.render("safari-for-payment-approval", {
                        safaries: safariForPaymentApproval,
                        station: station,
                        institution: institution
                    });
                }

                if(request.params.approvalStage === "payment_office"){ // expenditure unit
                    if(!station.isCashOffice){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("back");
                    }

                    const safariForPayment = safaries.filter(safari => {
                        return safari.approvalStage === "payment_office" && safari.isOfficial;
                    });

                    return response.render("safari-for-payment", {
                        safaries: safariForPayment,
                        station: station,
                        institution: institution
                    });
                }
            });
        });
    });
}

/***
 * Open safari advice to see safari details
 * @param request
 * @param response
 * @param next
 * @return response
 */
function viewSafariDetails(request, response, next){
    Station.findById(request.user.station, function(error, station){
       if(error){ return next(error); }

       if(!station){
           request.flash("error", "We cant find your station, please contact administrator, " +
               "or if you're the administrator the edit your profile to include the station");
           return response.redirect('/home');
       }

       Safari.findById(request.params.safariId)
           .populate("staff").populate("permissionStation").populate("warrantHoldingStation")
           .exec(function (error, safari){
               if(error){ return next(error); }

               if(!safari){
                   request.flash("error", "Something went wrong, please contact administrator");
                   return response.redirect("back");
               }

               Staff.findById(safari.staff.id).populate("station")
                   .populate("designation").populate("perdiemRate").exec(function(error, staff){
                   return response.render("safari-details", {
                       station: station,
                       safari: safari,
                       staff: staff
                   });
               });
       });
    });

}

/***
 * Launch delete confirmation page
 * @param request
 * @param response
 * @param next
 * @return response
 */
function deleteSafari(request, response, next){
    Safari.findById(request.params.safariId).populate("staff").exec(function (error, safari){
        if(error){ return next(error); }

        if(!safari){
            request.flash("error", "Something went wrong, please contact administrator");
            return response.redirect("back");
        }

        if(request.user.id !== safari.staff.id){
            request.flash("error", "Unauthorized action!");
            return response.redirect("home");
        }

        if(String(safari.approvalStage) === "supervisor" && !safari.approved){
            return response.render("confirm-safari-deletion", {
                safari: safari
            });
        } else {
            request.flash("error", "Safari is already approved in some stages, you can not delete, try cancel");
            return response.redirect("back");
        }
    });
}

/***
 * Process safari deletion
 * @param request
 * @param response
 * @param next
 * @return response
 */
function processDeletionOfSafari(request, response, next){
    Safari.findById(request.params.safariId).populate("staff").exec(function (error, safari){
        if(error) { return next(error); }

        if(!safari){
            request.flash("error", "Something went wrong, please contact administrator");
            return response.redirect("/home");
        }

        if(request.user.id !== safari.staff.id && safari.approval !== "pending"){
            request.flash("error", "Unauthorized action");
            return response.redirect("/home");
        }

        safari.deleted = true;
        safari.markModified("deleted");
        safari.save(function (error){
            if(error){
                let errorMessages = [];
                for(let key in error.errors){
                    errorMessages.push(error.errors[key].message);
                }
                request.flash("error", errorMessages);
                return response.redirect("back");
            }
            request.flash("info", "Safari is successfully deleted");
            return response.redirect("/home");
        });
    });
}

/***
 * Launch safari approval confirmation page
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function approveSafari(request, response, next){

    Safari.findById(request.params.safariId).populate("staff").exec(function (error, safari){
        if(error){ return next(error); }

        if(!safari){
            request.flash("error", "Something went wrong, please contact administrator");
            return response.redirect("/home");
        }

        return response.render("confirm-safari-approval", {
            safari: safari
        });
    });
}


function processSafariApproval(request, response, next){
    if(!request.user.isStationHead){
        request.flash("error", "Unauthorized access");
        return response.redirect("/home");
    }

    Station.findById(request.user.station, function (error, station){
        if(error){ return next(error); }

        Safari.findById(request.params.safariId, function (error, safari){
            if(error){ return next(error); }

            if(!safari){
                request.flash("error", "Something went wrong, we are can not retrieve the requested safari, " +
                    "please contact administrator");
                return response.redirect("/home");
            }

            let approvalMessage;

            switch (safari.approvalStage){
                case "supervisor":
                    approvalMessage = "approved permission of the safari";
                    approve(safari, request);
                    break;
                case "financial_approval":
                    if(!request.user.isWarrantHolder){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("/home");
                    }
                    approvalMessage = "approved warrant of fund of the safari";
                    approve(safari, request);
                    break;
                case "payment":
                    if(!station.isPaymentManager){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("/home");
                    }
                    approvalMessage = "approved payment for the safari";
                    approve(safari, request);
                    break;
                case "payment_office":
                    if(!station.isCashOffice){
                        request.flash("error", "Unauthorized action");
                        return response.redirect("/home");
                    }
                    approvalMessage = "you paid the safari";
                    approve(safari, request);
                    break;
            }

            safari.save(function (error, safari){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }
                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }
                request.flash("info", `You have successfully ${approvalMessage}`);
                return response.redirect("/view-safari-details/"+safari.id);
            });
        });
    });


}

/**
 * Launch safari forwarding confirmation
 * @param request
 * @param response
 * @param next
 * @return {*}
 */
function forwardSafariForApproval(request, response, next){
    if(!request.user.isStationHead || !request.user.isWarrantHolder){
        request.flash("error", "Unauthorized action");
        return response.redirect("/home");
    }

    Safari.findById(request.params.safariId).populate("staff").exec(function (error, safari){
        if(error){ return next(error); }

        if(!safari){
            request.flash("error", "Something went wrong, we can not find requested safari");
            return response.redirect("/home");
        }

        return response.render("confirm-forward-safari", { safari: safari });
    });
}

function processSafariForwarding(request, response, next){
    if(!request.user.isStationHead){
        request.flash("error", "Unauthorized action");
        return response.redirect("/home");
    }
    Station.findById(request.user.station, function (error, station){
        if(error){ return next(error); }

        Safari.findById(request.params.safariId, function (error, safari){
            if(error){ return next(error); }

            if(!safari){
                request.flash("error", "Something went wrong, we are can not retrieve the requested safari, " +
                    "please contact administrator");
                return response.redirect("/home");
            }

            let forwardingMessage;
            const previousApprovalStage = safari.approvalStage;

            switch (safari.approvalStage){
                case "supervisor":
                    safari.approvalStage = "financial_approval";
                    forwardingMessage = "forwarded safari to warrant of fund approval";
                    break;
                case "financial_approval":
                    if(!request.user.isWarrantHolder){
                        request.flash("error", "Unauthorized access");
                        return response.redirect("/home");
                    }
                    safari.approvalStage = "payment";
                    forwardingMessage = "forwarded safari to payment approval";
                    break;
                case "payment":
                    if(!station.isPaymentManager){
                        request.flash("error", "Unauthorized access");
                        return response.redirect("/home");
                    }
                    safari.approvalStage = "payment_office";
                    forwardingMessage = "forwarded safari to cash office for payments";
                    break;
            }

            safari.forwardedBy = `${request.user.firstName} ${request.user.lastName}`;
            safari.approval = "pending";
            safari.approvalHistory.push({
                approvalStage: safari.approvalStage,
                approvalStatus: "pending",
                date: new Date()
            });

            safari.markModified("approvalStage");
            safari.markModified("forwardedBy");
            safari.markModified("approval");

            safari.save(function (error){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }
                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }
                request.flash("info", `You have successfully ${forwardingMessage}`);
                return response.redirect("/load-safari-for-approval/"+previousApprovalStage);
            });

        });
    });
}

function editSafari(request, response, next){
    const destinations = [
        "Dar es Salaam", "Coastal Region", "Morogoro", "Tanga", "Kilimanjaro", "Arusha", "Manyara", "Singida", "Dodoma",
        "Iringa", "Njombe", "Songea", "Mbeya", "Rukwa", "Katavi", "Kigoma", "Tabora", "Shinyanga", "Kahama", "Mwanza"
    ];

    Institution.findById(request.user.institution, function (error, institution){
        if(error){ return next(error); }

        if(!institution){
            request.flash("error", "It look like we can not find your institution, please contact administrator");
            return response.redirect("/home");
        }

        Station.find({institution: institution}).populate("topStation").exec(function(error, stations){
            if(error){ return next(error); }

            if(stations.length === 0){
                request.flash("error", "Unexpected error, we're unable to load stations");
                return response.redirect("/home");
            }

            const userStation = stations.filter(station => {
                return station.id === String(request.user.station);
            });

            // const query = Safari.findById(request.params.safariId);
            // query.then( safari => {
            //
            // }).catch(error => next(error));

            Safari.findById(request.params.safariId, function (error, safari){
                if(error){ return next(error); }
                if(safari){

                    if(!(safari.approvalStage === "supervisor" && safari.approval === "pending")){
                        request.flash("error", "The safari has already been approved at some of the approval stages, therefore you can " +
                            "not edit unless the approval authorities send it back to you");
                        return response.redirect("back");
                    }

                    const warrantHoldingStation = stations.filter(station => {
                        return station.id === String(safari.warrantHoldingStation);
                    });

                    return response.render("edit-safari", {
                        csrfToken: request.csrfToken(),
                        safari: safari,
                        destinations: destinations,
                        userStation: userStation,
                        stations: stations,
                        warrantHoldingStation: warrantHoldingStation[0].name
                    });
                }
            });
        });
    });
}

/**
 * Process edited safari
 * @param request
 * @param response
 * @param next
 */
function processSafariEditing(request, response, next){
    // handle dates
    const departureDate = new Date(request.body.departuredate);
    const returnDate = new Date(request.body.returndate);
    const today = new Date();

    const halfPerDiem = request.body.isHalfPerDiem === "YES";
    const isOfficial = request.body.isOfficial === "YES";


    Safari.find({
        staff: request.user,
        deleted: false,
        departureDate: { $lte: departureDate },
        returnDate: { $gte: departureDate },
        _id: { $ne: request.body.safariId }
    }).exec( function(error, safaries){
        if(error){ return next(error); }

        if(safaries.length > 0){
            request.flash("error", "There is a conflict in dates of other safaries in your profile");
            return response.redirect("back");
        }

        Safari.findById(request.body.safariId, function (error, safari){
            if(error){ return next(error); }

            if(!safari){
                request.flash("error", "Something went wrong, unfortunately we can no find the requested safari!");
                return response.redirect("back");
            }

            checkDuplicatedSafari(request, response, departureDate, returnDate, today);


            safari.destination = request.body.destination;
            safari.isHalfPerDiem = halfPerDiem;
            safari.isOfficial = isOfficial;
            safari.purpose = request.body.purpose;
            safari.departureDate = departureDate;
            safari.returnDate = returnDate;
            safari.warrantHoldingStation = request.body.warrantHoldingStation;

            safari.markModified("destination");
            safari.markModified("isHalfPerDiem");
            safari.markModified("isOfficial");
            safari.markModified("purpose");
            safari.markModified("departureDate");
            safari.markModified("returnDate");
            safari.markModified("warrantHoldingStation");

            safari.save(function (error){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }

                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }
                request.flash("info", "Successfully updated the safari");
                return response.redirect("home");
            });
        });
    });
}


module.exports = {
    createSafari, processCreationOfSafari,
    createOtherPayments, processCreationOfOtherPayments,
    loadSafariForApproval, viewSafariDetails,
    deleteSafari, processDeletionOfSafari,
    approveSafari, processSafariApproval,
    forwardSafariForApproval, processSafariForwarding,
    editSafari, processSafariEditing
};
