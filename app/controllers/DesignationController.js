const Institution = require("../models/InstitutionModel");
const Designation = require("../models/DesignationModel");
const { checkIfNotInstitutionAdmin } = require("../../utilities/utilities");
const {validationResult} = require("express-validator");

/**
 * View Created designations
 * @param request
 * @param response
 * @param next
 * @return response
 */
function viewDesignations(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    Institution.findOne({staff: request.user}, function (error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, it look like you are not a SysAdmin on any Institution");
            return response.redirect("/home");
        }

        Designation.find({ institution: institution },function (error, designations){
            if(error){ return next(error); }
            return response.render("view-designations", { designations: designations, institution: institution });

        });
    });
}

/**
 * Create designation
 * @param request
 * @param response
 * @param next
 * @return response
 */
function createDesignation(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    Institution.findOne({ staff: request.user }, function (error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, it look like you are not a SysAdmin on any Institution");
            return response.redirect("/home");
        }
        return response.render("create-new-designation", { csrfToken: request.csrfToken(), institution: institution });
    });
}

function processCreateDesignation(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    Institution.findOne({ staff: request.user }, function (error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, it look like you are not a SysAdmin on any Institution");
            return response.redirect("/home");
        }

        const errors = validationResult(request);
        if(!errors.isEmpty()){
            let errorMessages = [];
            errors.array().forEach(error => {
                errorMessages.push(`${error.msg}`);
            });
            request.flash("error", errorMessages);
            return response.redirect("back");
        }

        Designation.findOne({name: request.body.designationName, institution: institution}, function (error, designation){
            if(error){ return next(error); }

            if(designation){
                request.flash("error", "Designation name already exist");
                return response.redirect("back");
            }

            const newDesignation = new Designation({
                name: request.body.designationName,
                description: request.body.description,
                institution: institution
            });

            newDesignation.save(function (error){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }
                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }

                request.flash("info", "Successfully created new designation");
                return response.redirect("view-designations");
            });
        });
    });

}

module.exports = {
    viewDesignations,
    processCreateDesignation,
    createDesignation
};