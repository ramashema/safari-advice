const {validationResult} = require("express-validator");
const User = require("../models/StaffModel");

/**
 * Render the login page(login form)
 * @param request
 * @param response
 */
function loginPage(request, response){
    const today = new Date();
    const csrfToken = request.csrfToken();
    response.render("login", { csrfToken: csrfToken, year: today.getFullYear() });
}

/**
 * Render the registration page(registration form)
 * @param request
 * @param response
 */
function registerPage(request, response){
    const csrfToken = request.csrfToken();
    response.render("register", { csrfToken: csrfToken });
}

/**
 * Process the registration(register user/staff to the system)
 * @param request
 * @param response
 * @param next
 */
function processRegistration(request, response, next){
    const errors = validationResult(request);
    let errorMessages = [];

    if(!errors.isEmpty()){
        errors.array().forEach(error => {
            errorMessages.push(`${error.msg} on ${error.param}`);
        });

        request.flash("error", errorMessages);
        return response.redirect("/register");
    }

    const firstname = request.body.firstname;
    const secondname = request.body.secondname;
    const lastname = request.body.lastname;
    const email = request.body.email;
    const password = request.body.password;


    User.findOne({email: email}, function(error, user){
        if(error){
            return next(error);
        }

        if(user){
            request.flash("error", "Staff with that email already exist");
            return response.redirect("back");
        }

        const newUser = new User({
            firstName: firstname,
            secondName: secondname,
            lastName: lastname,
            email: email,
            password: password
        });

        newUser.save(function (error){
            if(error){
                let errorMessages = [];
                for(let key in error.errors){
                    errorMessages.push(error.errors[key].message);
                }
                request.flash("error", errorMessages);
                return response.redirect("back");
            }

            request.flash("info", "SysAdmin has been successfully created!");
            return response.redirect("/");


        });
    });
}

/**
 * Logout confirmation message  and user reaction
 * @param request
 * @param response
 */
function logout(request, response){
    response.render("confirmlogout");
}

/**
 * Process user login out of the system
 * @param request
 * @param response
 * @param next
 */
function processLogout(request, response, next){
    request.logout(function (error){
        if(error){ return next(error); }
        request.flash("info", "You are logged out");
        response.redirect("/");
    });
}

/***
 * Exports modules for use as controllers
 * @type {{registerPage: registerPage, processRegistration: processRegistration, loginPage: loginPage}}
 */
module.exports = {
    loginPage, registerPage, processRegistration, logout, processLogout
};
