const Institution = require("../models/InstitutionModel");
const Safari = require("../models/SafariModel");
const Station = require("../models/StationModel");
/**
 * Render the home page
 * @param request
 * @param response
 * @param next
 */
function homePage(request, response, next){
    Safari.find({staff: request.user, deleted: false, cancelled: false}, function (error, safaries){
        if(error){ return next(error); }

        // if user is the normal user
        if(!(request.user.isSysAdmin || request.user.isInstituteAdmin)){
            Institution.findById(request.user.institution,function (error, institution){
                if(error){ next(error); }
                if(!institution){
                    request.flash("error", "Something went wrong contact the system administrator");
                    request.logout(function (error){
                        if(error){ return next(error); }
                        return response.redirect("/");
                    });
                }
                Station.findById(request.user.station, function (error, station){
                    if(error){ return next(error); }
                    if(!station){
                        request.flash("error", "Something went wrong, we can not find your station");
                        return response.redirect("/");
                    }

                    return response.render("home", {
                        institution: institution,
                        safaries: safaries,
                        station: station
                    });
                });

            });
        } else {
            // check if the login request is coming from institution administrator
            if (request.user.isInstituteAdmin) {
                Institution.findOne({staff: request.user}, function (error, institution) {
                    if (error) {
                        next(error);
                    }
                    if (!institution) {
                        request.flash("error", "Something went wrong contact the system administrator");
                        request.logout(function (error){
                            if(error){ return next(error); }
                            return response.redirect("/");
                        });
                    }
                    return response.render("home", {institution: institution, safaries: safaries});
                });
            } else {
                //if request is coming from SysAdmin
                return response.render("home", { safaries: safaries });
            }
        }
    });
}
/**
 * Redirect the user back to previous page when they want to go back
 * @param request
 * @param response
 * @returns {*}
 */
function goBack(request, response){
    return response.redirect("back");
}

/**
 * Export the Controller functions to be used by the router
 * @type {{homePage: ((function(*, *, *): (*|undefined))|*)}}
 */
module.exports = {
    homePage, goBack
};
