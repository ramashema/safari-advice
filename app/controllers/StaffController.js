const Station = require("../models/StationModel");
const Institution = require("../models/InstitutionModel");
const Staff = require("../models/StaffModel");
const Designation = require("../models/DesignationModel");
const RatePerdiem = require("../models/RatePerdiemModel");
const { checkIfNotInstitutionAdmin } = require("../../utilities/utilities");
const {validationResult, body} = require("express-validator");

/**
 * View all staffs that belong to an institution
 * @param request
 * @param response
 * @param next
 */
function viewAllStaff(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    Institution.findOne({staff: request.user}).then(function (institution){
        Staff.find({institution: institution})
            .populate("station")
            .populate("designation")
            .exec()
            .then(function (staffs){
            return response.render("view-all-staff", {
               staffs: staffs, institution: institution
            });
        }).catch(error => next(error));
    }).catch(error => next(error));
}

/**
 * Launch create new staff form
 * @param request
 * @param response
 * @param next
 * @return response
 */
function createNewStaff(request, response, next){
    checkIfNotInstitutionAdmin(request, response);
    Institution.findOne({staff: request.user}).exec().then(function(institution){
        Station.find({ institution: institution }).then(function(stations){
            if(stations.length === 0){
                request.flash("error", "You must create stations before adding users");
                return response.redirect("back");
            }

            Designation.find({ institution: institution }).then(function(designations){
                    if(designations.length === 0){
                        request.flash("error", "You must create designations before adding users");
                        return response.redirect("back");
                    }

                    RatePerdiem.find({ institution: institution }).then(function(ratesPerdiem){
                        if(ratesPerdiem.length === 0){
                            request.flash("error", "You must create rates perdiem before adding users");
                            return response.redirect("back");
                        }

                        return response.render("create-new-staff", {
                            csrfToken: request.csrfToken(),
                            institution: institution,
                            stations: stations,
                            designations: designations,
                            ratesPerdiem: ratesPerdiem
                        });

                    }).catch(error => next(error));
                }).catch(error => next(error));
        }).catch(error => next(error));
    }).catch(error => next(error));
}

/**
 * Process new staff creation
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function processNewStaffCreation(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    const errors = validationResult(request);
    let errorMessages = [];

    if(!errors.isEmpty()){
        errors.array().forEach(error => {
            errorMessages.push(`${error.msg} on ${error.param}`);
        });

        request.flash("error", errorMessages);
        return response.redirect("back");
    }

    request.body.isStationHead = request.body.isStationHead === "YES";
    request.body.isWarrantHolder = request.body.isWarrantHolder === "YES";

    const staffs = Staff.find().populate("station").exec();

    staffs.then(function(staffs){
        /** check if there is any user with the email of the new user **/
        const isEmailInUse = staffs.some(staff => staff.email === request.body.email);

        /** check if the station already have station head , preventing having multiple station head**/
        const stationHead = staffs.filter(staff => {
            if(staff.station){
                if(staff.station.id === request.body.station){
                    return staff.isStationHead;
                }
            }
        });

        /** prevent same email to be added multiple times**/
        if(isEmailInUse){
            request.flash("error", "The email you choose is not available, the staff might already exists");
            return response.redirect("back");
        }

        /** prevent multiple station head in the same station **/
        if(stationHead.length > 0 && request.body.isStationHead){
            request.flash("error", "There must be only one station head in each station");
            return response.redirect("back");
        }

        /** get institution to be sent to the template **/
        const institution = Institution.findById(request.body.institutionId).exec();

        /** if the institution has been obtained **/
        institution.then(function(institution){

            /** check if the warrant holder is the station head **/
            if(request.body.isWarrantHolder && !request.body.isStationHead){
                request.flash("error", "A warrant holder must be a station head");
                return response.redirect("back");
            }

            /** create new staff details to be posted to the database **/
            const newStaff = new Staff({
                firstName: request.body.firstname,
                secondName: request.body.secondname,
                lastName: request.body.lastname,
                email: request.body.email,
                password: request.body.password,
                designation: request.body.designation,
                perdiemRate: request.body.perdiemrate,
                institution: institution,
                station: request.body.station,
                isStationHead: request.body.isStationHead,
                isWarrantHolder: request.body.isWarrantHolder,
                isSysAdmin: false
            });

            /** add new staff to the database **/
            const addStaff = newStaff.save();
            addStaff.then(function (staff){
                request.flash("info", `${staff.email} successfully created`);
                return response.redirect("view-all-staff");
            });

            addStaff.catch(function(error){
                // let errorMessages = [];
                // for(let key in error.errors){
                //     errorMessages.push(error.errors[key].message);
                // }
                request.flash("error", error);
                return response.redirect("back");
            });
        });

        institution.catch(function(error){
            return next(error);
        });
    });
    staffs.catch(function (error){
        return next(error);
    });
}


// here we need user edit and user disable function


module.exports = {
    viewAllStaff,
    createNewStaff,
    processNewStaffCreation
};
