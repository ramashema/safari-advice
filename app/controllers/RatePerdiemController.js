const Institution = require("../models/InstitutionModel");
const RatePerDiem = require("../models/RatePerdiemModel");
const { checkIfNotInstitutionAdmin } = require("../../utilities/utilities");
const {validationResult} = require("express-validator");


function viewRatesPerDiem(request, response, next){
    checkIfNotInstitutionAdmin(request, response);
    Institution.findOne({ staff: request.user }, function (error, institution){
       if(error){ return next(error); }

       if(!institution){
           request.flash("error", "Something went wrong, it look like we can not find your institution");
           return response.redirect("/home");
       }

       RatePerDiem.find({ institution: institution }, function (error, ratesPerDiem){
            if(error){ return next(error); }
            return response.render("view-rates-perdiem", { ratesPerDiem: ratesPerDiem, institution: institution });
       });
    });
}

function createRatePerDiem(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    Institution.findOne({ staff: request.user }, function (error, institution){
       if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, it look like we can not find your institution");
            return response.redirect("/home");
        }
        return response.render("create-new-rate-perdiem", { csrfToken: request.csrfToken(), institution: institution });
    });
}

function processRatePerDiemCreation(request, response, next){
    checkIfNotInstitutionAdmin(request, response);
    Institution.findOne({ staff: request.user }, function (error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, it look like we can not find your institution");
            return response.redirect("/home");
        }

        const errors = validationResult(request);
        if(!errors.isEmpty()){
            let errorMessages = [];
            errors.array().forEach(error => {
                errorMessages.push(error.msg);
            });
            request.flash("error", errorMessages);
            return response.redirect("back");
        }

        RatePerDiem.findOne({amount: request.body.amount, institution: institution}, function (error, ratePerDiem){
            if(error){ return next(error); }
            if(ratePerDiem){
                request.flash("error", "The rate you are trying to create already exist!");
                return response.redirect("back");
            }

            const newRatePerDiem = new RatePerDiem({
                amount: Number(request.body.amount).toFixed(2),
                description: request.body.description,
                institution: institution
            });

            newRatePerDiem.save(function (error){
                if(error){
                    let errorMessages = [];
                    for(let key in error.errors){
                        errorMessages.push(error.errors[key].message);
                    }
                    request.flash("error", errorMessages);
                    return response.redirect("back");
                }
                request.flash("info", "Successfully created new RatePerdiem");
                return response.redirect("/view-rates-perdiem");
            });
        });
    });
}

module.exports = {
    viewRatesPerDiem, createRatePerDiem, processRatePerDiemCreation
};