const Station = require("../models/StationModel");
const Institution = require("../models/InstitutionModel");
const {validationResult} = require("express-validator");
const { storeNewStation, checkIfNotInstitutionAdmin } = require("../../utilities/utilities");

/**
 * Launch station creation form
 * @param request
 * @param response
 * @param next
 */
function createStation(request, response, next){
    checkIfNotInstitutionAdmin(request, response);

    const institution = Institution.findOne({staff: request.user}).exec();

    institution.then(institution => {
        const station = Station.find({ institution: institution }).exec();
        station.then( stations => {
            return response.render("create-station", {
                csrfToken: request.csrfToken(),
                institution: institution,
                stations: stations
            });
        } );

        station.catch(error => next(error));
    });

    institution.catch(error => next(error));

    // Institution.findOne({ staff: request.user }, function (error, institution){
    //     if(error){ return next(error); }
    //     if(!institution){
    //         request.flash("error", "Something went wrong, problem when accessing your institution");
    //         return response.redirect("/home");
    //     }
    //     Station.find({ institution: institution }, function (error, stations){
    //         if(error){ return next(error); }
    //         return response.render("create-station",{
    //             csrfToken: request.csrfToken(),
    //             institution: institution,
    //             stations: stations
    //         });
    //     });
    // });
}
/**
 * Process creation of new station
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function processCreateStation(request, response, next){
    let message;
    let newStation;

    const errors = validationResult(request);

    checkIfNotInstitutionAdmin(request, response);
    if(!errors.isEmpty()){
        let errorMessages = [];
        errors.array().forEach(error=>{
            errorMessages.push(error.msg);
        });
        request.flash("error", errorMessages);
        return response.redirect("back");
    }

    const institution = Institution.findOne({staff: request.user}).exec();
    institution.then(institution => {
        if(institution.id !== request.body.institutionId){
            request.flash("error", "Unauthorized action, it seems that you're not an admin of "+institution.name);
            return response.redirect("/home");
        }

        const isPaymentManager = request.body.isPaymentManager === "YES";
        const isPaymentOffice = request.body.paymentOffice === "YES";


        if(request.body.topStation){
            newStation = new Station({
                name: request.body.stationname,
                shortCode: request.body.shortcode,
                headOfStationTitle: request.body.headofstationtitle,
                topStation: request.body.topStation,
                institution: institution,
                isTopStation: false,
                isPaymentManager: isPaymentManager,
                isCashOffice: isPaymentOffice
            });
        } else{
            newStation = new Station({
                name: request.body.stationname,
                shortCode: request.body.shortcode,
                headOfStationTitle: request.body.headofstationtitle,
                institution: institution,
                isTopStation: true
            });
        }

        const station = Station.find({institution: institution}).exec();
        station.then(stations => {
            if(stations.length === 0) {
                message = `Successfully created top station/main office for ${institution.name}`;
                storeNewStation(request, response, newStation, message, "back");
            }

            if(stations.length > 0){
                // Filter the station with similar name and to station
                const sameNameStation = stations.filter(station =>{ return station.name === newStation.name; });
                const isThereATopStation = stations.filter( station =>{ return station.isTopStation; });
                const isThereAPaymentManagementStation = stations.filter( station => { return station.isPaymentManager; });
                const isThereACashOfficeStation = stations.filter( station => { return station.isCashOffice; });
                //If there is the station with the same name, don't create the station
                if(sameNameStation.length > 0){
                    request.flash("error", `The station with the same name exist in ${institution.name}`);
                    return response.redirect("back");
                }
                //if they try to create multiple stations
                if(isThereATopStation.length > 0 && newStation.isTopStation){
                    request.flash("error", `You can not create multiple top stations offices in the same institution`);
                    return response.redirect("back");
                }
                // if they try to create multiple payment stations
                if(isThereAPaymentManagementStation.length > 0 && newStation.isPaymentManager  ){
                    request.flash("error", `You can not create multiple payment management stations in the same institution`);
                    return response.redirect("back");
                }
                // if they try to create multiple cash office
                if(isThereACashOfficeStation.length > 0 && newStation.isCashOffice ){
                    request.flash("error", `You can not create multiple cash offices in the same institution`);
                    return response.redirect("back");
                }

                message = "Successfully created station";
                storeNewStation(request, response, newStation, message, "back");
            }
        });
        station.catch(error => next(error));
    });
    institution.catch(error => next(error));

    // Institution.findOne({ staff: request.user }, function (error, institution){
    //     // if(institution.id !== request.body.institutionId){
    //     //     request.flash("error", "Unauthorized action, it seems that you're not an admin of "+institution.name);
    //     //     return response.redirect("/home");
    //     // }
    //
    //     // define the payment manager and payment office stations for payments approval
    //     // const isPaymentManager = request.body.isPaymentManager === "YES";
    //     // const isPaymentOffice = request.body.paymentOffice === "YES";
    //
    //     //Check if user included top station while filling the form
    //     // if(request.body.topStation){
    //     //      newStation = new Station({
    //     //          name: request.body.stationname,
    //     //          shortCode: request.body.shortcode,
    //     //          headOfStationTitle: request.body.headofstationtitle,
    //     //          topStation: request.body.topStation,
    //     //          institution: institution,
    //     //          isTopStation: false,
    //     //          isPaymentManager: isPaymentManager,
    //     //          isCashOffice: isPaymentOffice
    //     //     });
    //     // } else{
    //     //     newStation = new Station({
    //     //         name: request.body.stationname,
    //     //         shortCode: request.body.shortcode,
    //     //         headOfStationTitle: request.body.headofstationtitle,
    //     //         institution: institution,
    //     //         isTopStation: true
    //     //     });
    //     // }
    //     //Get stations on the particular institution
    //     // Station.find({ institution: institution }, function (error, stations){
    //     //     // if(error){ return next(error); }
    //     //     // //If there is no any station in the institution then create main station/main office
    //     //     // if(stations.length === 0) {
    //     //     //     message = `Successfully created top station/main office for ${institution.name}`;
    //     //     //     storeNewStation(request, response, newStation, message, "back");
    //     //     // }
    //     //     //If there are stations already available in an institution
    //     //     // if(stations.length > 0){
    //     //     //     // Filter the station with similar name and to station
    //     //     //     const sameNameStation = stations.filter(station =>{ return station.name === newStation.name; });
    //     //     //     const isThereATopStation = stations.filter( station =>{ return station.isTopStation; });
    //     //     //     const isThereAPaymentManagementStation = stations.filter( station => { return station.isPaymentManager; });
    //     //     //     const isThereACashOfficeStation = stations.filter( station => { return station.isCashOffice; });
    //     //     //     //If there is the station with the same name, don't create the station
    //     //     //     if(sameNameStation.length > 0){
    //     //     //         request.flash("error", `The station with the same name exist in ${institution.name}`);
    //     //     //         return response.redirect("back");
    //     //     //     }
    //     //     //     //if they try to create multiple stations
    //     //     //     if(isThereATopStation.length > 0 && newStation.isTopStation){
    //     //     //        request.flash("error", `You can not create multiple top stations offices in the same institution`);
    //     //     //        return response.redirect("back");
    //     //     //     }
    //     //     //     // if they try to create multiple payment stations
    //     //     //     if(isThereAPaymentManagementStation.length > 0 && newStation.isPaymentManager  ){
    //     //     //         request.flash("error", `You can not create multiple payment management stations in the same institution`);
    //     //     //         return response.redirect("back");
    //     //     //     }
    //     //     //     // if they try to create multiple cash office
    //     //     //     if(isThereACashOfficeStation.length > 0 && newStation.isCashOffice ){
    //     //     //         request.flash("error", `You can not create multiple cash offices in the same institution`);
    //     //     //         return response.redirect("back");
    //     //     //     }
    //     //     //
    //     //     //     message = "Successfully created station";
    //     //     //     storeNewStation(request, response, newStation, message, "back");
    //     //     // }
    //     // });
    // });
}

module.exports = { createStation, processCreateStation };
