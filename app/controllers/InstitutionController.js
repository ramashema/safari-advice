const { validationResult } = require("express-validator");


const Institution = require("../models/InstitutionModel");
const InstitutionSysAdmin = require("../models/StaffModel");


// utility function that will be copied to another file
function checkIfNotSysAdmin(request, response){
    if(!request.user.isSysAdmin){
        request.flash("error", "You are not authorized to perform this action");
        return response.redirect("/home");
    }
}
/**
 * Render the institution form
 * @param {*} request 
 * @param {*} response 
 */
function createInstitution(request, response){
    checkIfNotSysAdmin(request, response);
    return response.render("create-institution", { csrfToken: request.csrfToken() });
}
/**
 * Process Institute creation
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function processCreateInstitution(request, response, next){
    checkIfNotSysAdmin(request, response);

    const errors = validationResult(request);
    if(!errors.isEmpty()){
        let errorMessages = [];
        errors.array().forEach(error => {
            errorMessages.push(`${error.msg}`);
        });

        request.flash("error", errorMessages);
        return response.redirect("back");
    }

    Institution.findOne({name: request.body.institutionname}, (error, institution) =>{
        if(error){ return next(error); }
        if(institution){
            request.flash("error", "Institution with the same name already exit!");
            return response.redirect("back");
        }

        const newInstitution = new Institution({
            name: request.body.institutionname,
            shortCode: (request.body.shortcode).trim().toUpperCase()
        });

        newInstitution.save( error => {
            if(error){
                console.log(error);
                let errorMessages = [];
                for(let key in error.errors){
                    errorMessages.push(error.errors[key].message);
                }
                request.flash("error", errorMessages);
                return response.redirect("back");
            }
            return response.redirect("/create-institution-sysadmin/"+newInstitution.id);
        });
    });
}
/***
 * Render the institution SysAdmin Creation form
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function createInstitutionSysAdmin(request, response, next){
    checkIfNotSysAdmin(request, response);

    Institution.findById(request.params.institutionId, function(error, institution){
        if(error){ return next(error); }
        if(!institution){
            request.flash("error", "Something went wrong, Institution not found");
            return response.redirect("/home");
        }
        return response.render(
            "create-institution-sysadmin", {
                csrfToken: request.csrfToken(),
                institution: institution
            });
    });
}
/***
 * Process the institution SysAdmin Creation Request
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function processCreationOfInstitutionSysAdmin(request, response, next){
    checkIfNotSysAdmin(request, response);

    const errors = validationResult(request);
    if(!errors.isEmpty()){
        let errorMessages = [];
        errors.array().forEach(error => {
            errorMessages.push(`${error.msg}`);
        });
        request.flash("error", errorMessages);
        return response.redirect("/create-institution-sysadmin/"+request.body.institution);
    }

    InstitutionSysAdmin.findOne({email: request.body.email}, function (error, staff){
        if(error){ return next(error); }
        if(staff){
            request.flash("error", "User already exist, you can't add an existing user as admin of newly created Institute");
            return response.redirect("/create-institution-sysadmin/"+request.body.institution);
        }

        const institutionSysAdmin = new InstitutionSysAdmin({
            firstName: request.body.firstname,
            secondName: request.body.secondname,
            lastName: request.body.lastname,
            email: request.body.email,
            password: request.body.password,
            institution: request.body.institution,
            isSysAdmin: false,
            isInstituteAdmin: true
        });

        institutionSysAdmin.save(error => {
            if(error){
                let errorMessages = [];
                for(let key in error.errors){
                    errorMessages.push(error.errors[key].message);
                }
                request.flash("error", errorMessages);
                return response.redirect("/create-institution-sysadmin/"+request.body.institution);
            }

            Institution.findById(request.body.institution, (error, institution) => {
                if(error){
                    return next(error);
                }

                if(!institution){
                    request.flash("error", "Something is not right, we can't find the institution");
                    return response.redirect("/home");
                }

                institution.staff = institutionSysAdmin;
                institution.updatedAt = new Date();

                institution.markModified("staff");
                institution.markModified("updatedAt");

                institution.save(error => {
                    if(error){
                        let errorMessages = [];
                        for(let key in error.errors){
                            errorMessages.push(error.errors[key].message);
                        }

                        Institution.findByIdAndRemove(institution.id, function (error){
                            if(error){ return next(error); }

                            errorMessages.push("Since there is an error adding SysAdmin, we have remove an associated Institution");
                            request.flash("error", errorMessages);
                            return response.redirect("/home");
                        });
                    }
                    request.flash("info", "Successful created "+institution.name+" and its SysAdmin");
                    return response.redirect("/view-institutions");
                });
            });
        });
    });
}
/***
 * View the list of institution available in the database
 * @param request
 * @param response
 * @param next
 * @returns {*}
 */
function viewInstitutions(request, response, next){
    checkIfNotSysAdmin(request, response);
    Institution.find().populate("staff").exec((error, institutions) => {
        if(error){
            return next(error);
        }
        return response.render("view-institutions", { institutions: institutions});
    });
}
/***
 * View the details of an institution
 * @param request
 * @param response
 * @returns {*}
 */
function institutionDetails(request, response){
    checkIfNotSysAdmin(request, response);
    Institution.findById(request.params.id).populate("staff").exec((error, institution) => {
        if(error){
            return next(error);
        }
        if(!institution){
            request.flash("error", "Institution not found");
            return response.redirect("back");
        }
        return response.render("institution-details", { institution: institution});
    });
}

/***
 * Exports the Controllers to be used in the routers
 * @type {{createInstitution: ((function(*, *): (*))|*), institutionDetails: ((function(*, *): *)|*), viewInstitutions: ((function(*, *, *): *)|*), processCreationOfInstitutionSysAdmin: ((function(*, *, *): *)|*), processCreateInstitution: ((function(*, *, *): (*|undefined))|*), createInstitutionSysAdmin: ((function(*, *, *): *)|*)}}
 */
module.exports = {
    createInstitution,
    processCreateInstitution,
    createInstitutionSysAdmin,
    processCreationOfInstitutionSysAdmin,
    viewInstitutions,
    institutionDetails
};
