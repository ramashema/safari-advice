const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const csrf = require("csurf");

const { loginPage } = require("../controllers/AuthenticationController");
const { homePage } = require("../controllers/PrivatePagesController");
const { ensureAuthenticated } = require("../../utilities/utilities");
const { createInstitution, processCreateInstitution,
        createInstitutionSysAdmin, processCreationOfInstitutionSysAdmin,
        viewInstitutions, institutionDetails } = require("../controllers/InstitutionController");
const { createStation, processCreateStation } = require("../controllers/StationController");
const { goBack } = require("../controllers/PrivatePagesController");
const { viewAllStaff, createNewStaff, processNewStaffCreation } = require("../controllers/StaffController");
const { viewDesignations, createDesignation,
        processCreateDesignation } = require("../controllers/DesignationController");
const { viewRatesPerDiem, createRatePerDiem,
        processRatePerDiemCreation } = require("../controllers/RatePerdiemController");
const { createSafari, processCreationOfSafari,
        createOtherPayments, processCreationOfOtherPayments,
        loadSafariForApproval, viewSafariDetails, deleteSafari,
        processDeletionOfSafari, approveSafari, processSafariApproval,
        forwardSafariForApproval, processSafariForwarding, editSafari,
        processSafariEditing } = require("../controllers/SafariController");


router.use(function(request, response, next){
    response.locals.currentUser = request.user;
    response.locals.errors = request.flash("error");
    response.locals.infos = request.flash("info");
    next();
});
router.use(csrf());
router.get("/", loginPage);
router.get("/home", ensureAuthenticated , homePage);
router.get("/create-institution", ensureAuthenticated, createInstitution);
router.post("/create-institution",
    body("institutionname").isLength({min:3}).withMessage("Institution name is too short"),
    body("shortcode").isLength({max:5}).withMessage("Short code is too long to be stored"),
    ensureAuthenticated,
    processCreateInstitution
);
router.get("/create-institution-sysadmin/:institutionId", ensureAuthenticated, createInstitutionSysAdmin);
router.post("/create-institution-sysadmin",
    body('email').isEmail().withMessage("Email should be valid email address"),
    body('password').isAlphanumeric().isLength({min: 8}).withMessage("Password should be 8 character or more"),
    ensureAuthenticated,
    processCreationOfInstitutionSysAdmin
);
router.get("/view-institutions", ensureAuthenticated, viewInstitutions);
router.get("/institution-details/:id", ensureAuthenticated, institutionDetails);
router.get("/create-station", ensureAuthenticated, createStation );
router.post("/create-station",
    ensureAuthenticated,
    body("stationname").isLength({min: 3}).withMessage("Station name is too short"),
    body("headofstationtitle").isLength({min: 3}).withMessage("Head of station is too short"),
    body("shortcode").isLength({max: 8}).withMessage("The shortcode should not be too long"),
    processCreateStation
);
router.get("/view-all-staff", ensureAuthenticated, viewAllStaff);
router.get("/create-new-staff/:institutionId", ensureAuthenticated, createNewStaff);
router.post("/create-new-staff",
    ensureAuthenticated,
    body('email').isEmail(),
    body('password').isAlphanumeric().isLength({min: 8}).withMessage("Password should be at least 8 characters long"),
    processNewStaffCreation);
router.get("/view-designations", ensureAuthenticated, viewDesignations);
router.get("/create-new-designation", ensureAuthenticated, createDesignation );
router.post("/create-new-designation",
    ensureAuthenticated,
    body("designationName").isLength({min: 3}).withMessage("Designation name is too short"),
    processCreateDesignation
);
router.get("/view-rates-perdiem", ensureAuthenticated, viewRatesPerDiem );
router.get("/create-new-rate-perdiem", ensureAuthenticated, createRatePerDiem );
router.post("/create-new-rate-perdiem",
    ensureAuthenticated,
    body("amount").isNumeric().withMessage("Amount must be a number"),
    body("description").isLength({min: 5}).withMessage("RatePerdiem descriptions too short"),
    processRatePerDiemCreation
);
router.get("/create-new-safari", ensureAuthenticated, createSafari);
router.post("/create-new-safari",
    ensureAuthenticated,
    body("destination").isLength({min: 1}).withMessage("Destination is required"),
    body("isOfficial").isLength({min: 1}).withMessage("Safari should be either of the two, official or not official"),
    body("purpose").isLength({min: 1}).withMessage("Purpose field is required"),
    body("departuredate").isDate().withMessage("Departure date must be a valid date"),
    body("returndate").isDate().withMessage("Return date must be a valid date"),
    body("warrantHoldingStation").isLength({min: 1}).withMessage("Warrant holding station must be specified"),
    processCreationOfSafari
);
router.get("/create-other-payments/:safariId/:staffId", ensureAuthenticated, createOtherPayments);
router.post("/create-other-payments/",
    ensureAuthenticated,
    body("paymentname").isLength({ min: 5 }).withMessage("Payment name is required"),
    body("description").isLength({ min: 5 }).withMessage("Payment description is required"),
    processCreationOfOtherPayments
);

router.get("/load-safari-for-approval/:approvalStage", ensureAuthenticated, loadSafariForApproval );
router.get("/view-safari-details/:safariId", ensureAuthenticated, viewSafariDetails);
router.get("/confirm-delete-safari/:safariId", ensureAuthenticated, deleteSafari);
router.get("/delete-safari/:safariId", ensureAuthenticated, processDeletionOfSafari);
router.get("/confirm-approve-safari/:safariId", ensureAuthenticated, approveSafari);
router.get("/approve-safari/:safariId", ensureAuthenticated, processSafariApproval);
router.get("/confirm-forward-safari/:safariId", ensureAuthenticated, forwardSafariForApproval );
router.get("/forward-safari/:safariId", ensureAuthenticated, processSafariForwarding );
router.get("/edit-safari/:safariId", ensureAuthenticated, editSafari);
router.post("/update-safari",
    ensureAuthenticated,
    body("destination").isLength({min: 1}).withMessage("Destination is required"),
    body("isOfficial").isLength({min: 1}).withMessage("Safari should be either of the two, official or not official"),
    body("purpose").isLength({min: 1}).withMessage("Purpose field is required"),
    body("departuredate").isDate().withMessage("Departure date must be a valid date"),
    body("returndate").isDate().withMessage("Return date must be a valid date"),
    body("warrantHoldingStation").isLength({min: 1}).withMessage("Warrant holding station must be specified"),
    processSafariEditing
);

router.get("/go-back", ensureAuthenticated, goBack);

module.exports = router;
