const express = require("express");
const passport = require("passport");
const { body } = require("express-validator");
const csrf = require("csurf");
const authRouter = express.Router();

const { loginPage, registerPage, processRegistration,
        logout, processLogout } = require("../controllers/AuthenticationController");
const {ensureAuthenticated} = require("../../utilities/utilities");

authRouter.get("/login", loginPage );
authRouter.post("/login",
    body('email').isEmail().withMessage('Please enter a valid email address'),
    body('password').isAlphanumeric().withMessage('Password must be alphanumeric'),
    body('password').isLength({min: 8}).withMessage('Please enter a password with at least 8 characters'),
    passport.authenticate("local", {
    successRedirect: "/home",
    failureRedirect: "/",
    failureFlash: true
}));
authRouter.get("/logout", ensureAuthenticated, logout);
authRouter.get("/confirmlogout", ensureAuthenticated, processLogout);

authRouter.use(csrf());
authRouter.get("/register", registerPage);
authRouter.post(
    "/register",
    body('email').isEmail(),
    body('password').isAlphanumeric(),
    processRegistration
);

module.exports = authRouter;
