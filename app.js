const express = require("express");
const logger = require("morgan");
const path = require("path");
const createHTTPErrors = require("http-errors");
const mongoose = require("mongoose");
const mongoSanitize = require("express-mongo-sanitize");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const passport = require("passport");
const flash = require("connect-flash");

require("dotenv").config();
const router = require("./app/routes/router");
const authRouter = require("./app/routes/auth");
const { gracefulShutdown } = require("./utilities/utilities");
const setUpPassport = require("./utilities/passportSetUp");

const app = express();
const VIEWS = path.join(__dirname, "app", "views");
const STATIC_FILES = path.join(__dirname, "static");

let dbURL = process.env.DOCKER_MONGO_DB;
if(process.env.NODE_ENV === "production"){
    dbURL = process.env.LIVE_DB_URI;
    mongoose.connect(dbURL, {useNewURLParser: true});
}

mongoose.connect(dbURL, {dbName: 'safari-advice', useNewUrlParser: true});
mongoose.connection.on("connected", function(){
    console.log("connected to mongodb://"+dbURL);
});
mongoose.connection.on("error", function(error){
    console.log("mongodb connection error: "+error);
});
mongoose.connection.on("disconnected", function(){
    console.log("mongodb has been disconnected");
});

process.once("SIGUSR2", function(){
    gracefulShutdown("nodemon restart", function(){
        process.kill(process.pid, "SIGUSR2");
    });
});
process.on("SIGINT", function(){
    gracefulShutdown("app termination", function(){
        process.exit(0);
    });
});
process.on("SIGTERM", function(){
    gracefulShutdown("heroku app shutdown", function(){
        process.exit(0);
    });
});

setUpPassport();
app.set("PORT", process.env.PORT || 8080);
app.set("views", VIEWS);
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.urlencoded({extended:true}));
app.use(mongoSanitize({
    allowDots: true,
    onSanitize: function (request, key){
        console.warn(`${key} has been sanitized`);
    }
}));
app.use(cookieParser());
app.use(session({
    secret: "*&NBH567%SM_++$%&#?nDTHsndu484*^^^$#FRYVV",
    resave: true,
    saveUninitialized: true,
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(router);
app.use(authRouter);
app.use(express.static(STATIC_FILES));
app.use(function(request, response, next){
    next(createHTTPErrors(404));
});

app.use(function(error, request, response,next){
    const message = error.message;
    error = process.env.NODE_ENV === 'development' ? error : {};

    response.status(error.status || 500);
    response.render("errors", {
        error: error,
        message: message
    });
});


app.listen(app.get("PORT"), function (){
    console.log("safari advice server is running on port: "+app.get("PORT"));
});
