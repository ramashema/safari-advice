FROM node

ENV MONGO_DB_USERNAME=root MONGO_DB_PWD=root

RUN mkdir -p /home/app

WORKDIR /home/app

COPY . /home/app

RUN npm install

RUN npm rebuild bcrypt --build-from-source

CMD ["node", "/home/app/app.js"]