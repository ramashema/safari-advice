const mongoose = require("mongoose");

/**
 * Graceful close mongoose connection on different events
 * @param message
 * @param callback
 */
function gracefulShutdown(message, callback){
    mongoose.connection.close(function(){
        console.log("connection to database has been closed due to: "+ message);
        callback();
    });
}
/**
 * Ensure user is authenticated before accessing private pages
 * @param request
 * @param response
 * @param next
 */
function ensureAuthenticated(request, response, next){
    if(request.isAuthenticated()){
        return next();
    } else {
        request.flash("error", "Please login first");
        response.redirect("/login");
    }
}
/**
 * Store newStation
 * @param request
 * @param response
 * @param station
 * @param message
 * @param redirectTo
 */
function storeNewStation(request, response, station, message, redirectTo){
    station.save( error => {
        if(error){
            console.log("The error: "+error);
            let errorMessages = [];
            for(let key in error.errors){
                errorMessages.push(error.errors[key].message);
            }
            request.flash("error", errorMessages);
            return response.redirect("back");
        }
        request.flash("info", message);
        return response.redirect(redirectTo);
    });
}
/**
 * Check if current logged-in user is an institution administrator
 * @param request
 * @param response
 * @returns {*}
 */
function checkIfNotInstitutionAdmin(request, response){
    if(!request.user.isInstituteAdmin){
        request.flash("error", "Unauthorized Action!");
        return response.redirect("/home");
    }
}


module.exports = {
    gracefulShutdown,
    ensureAuthenticated,
    storeNewStation,
    checkIfNotInstitutionAdmin,
};
