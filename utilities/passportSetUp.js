const passport = require("passport");
const LocalStrategy = require("passport-local");
const User = require("../app/models/StaffModel");

module.exports = function (){
    //serialize user - extract user id from user object
    passport.serializeUser(function(user, next){
        next(null, user._id);
    });

    //deserialize user - get user object from user id
    passport.deserializeUser(function (id, next){
        User.findById(id, function(error, user){
            next(error, user);
        });
    });
};

passport.use("local", new LocalStrategy({
    usernameField: 'email',
},function (username, password, next){
    User.findOne({email: username}, function(error, user){
        if(error){
            next(error);
        }

        if(!user){
            return next(null, false,{
                message: "Invalid username"
            });
        }

        user.checkPassword(password, function(error, isMatch){
            if(error){
                next(error);
            }
            if(!isMatch){
                return next(null, false, {
                    message: "Invalid password"
                });
            } else{
                next(null, user);
            }
        });
    });
}));
